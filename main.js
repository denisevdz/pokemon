window.onload = () => {

  const resetPokemon = document.getElementById('js--box');
  const pokemonPicture = document.getElementById('js--pokemon-picture');
  const pokemonPicture2 = document.getElementById('js--pokemon-picture2');
  const pokemonPicture3 = document.getElementById('js--pokemon-picture3');
  const pokemonName = document.getElementById('js--pokemon');
  const pokemonName2 = document.getElementById('js--pokemon2');
  const pokemonName3 = document.getElementById('js--pokemon3');

  const pokemonStarterSprites = document.getElementsByClassName('js--pokemonStarterSprite');
  const pokemonNames = document.getElementsByClassName('js--starterName');
  let starterPokemon;

  let battleStarted = true;
  const wildPokemonSprite = document.getElementsByClassName('js--pokemonWildSprite');
  const wildName = document.getElementsByClassName('js--wildName');
  const wildPokemon = document.getElementById('wildPokemon');

  const attackMoves1 = document.getElementsByClassName('js--attackMoves1');
  const attackMoves2 = document.getElementsByClassName('js--attackMoves2');
  const attackMoves3 = document.getElementsByClassName('js--attackMoves3');
  const attackMoves4 = document.getElementsByClassName('js--attackMoves4');
  const attackMoves5 = document.getElementsByClassName('js--attackMoves5');

  const starterBattle = document.getElementById('50');
  const starterBattleName = document.getElementById('js--name');

  const setupStarterPokemon = (number, index) => {
    for (let i = 0; i < pokemonStarterSprites.length; i++) {
      const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

      fetch(BASE_URL + number).then((data) => {
        return data.json();
      })
      .then( (response) => {
        pokemonStarterSprites[index].setAttribute("src", response.sprites.front_default);
        pokemonNames[index].setAttribute("value", response.name);
        const turned = response.sprites.back_default;


      });
    }
  }


  const setupWildPokemon = () => {
    for (let i = 0; i < wildPokemonSprite.length; i++) {
      let number = Math.floor(Math.random() * 151 + 1);
      const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

      fetch(BASE_URL + number).then((data) => {
        return data.json();
      })
      .then( (response) => {
        if(i == 1){
          wildPokemonSprite[i].setAttribute("src", response.sprites.front_shiny);
          wildName[i].setAttribute("value", response.name);
          wildPokemonSprite[i].setAttribute("alt", number);
        }else{
        wildPokemonSprite[i].setAttribute("src", response.sprites.front_default);
        wildName[i].setAttribute("value", response.name);
        wildPokemonSprite[i].setAttribute("alt", number);
      }

      });
    }
  }
  for (let i = 0; i < wildPokemonSprite.length; i++) {
    wildPokemonSprite[i].onmouseenter = (event) => {
      setupMoves(wildPokemonSprite[i].getAttribute("alt"), wildPokemonSprite[i]);
    }
  }

  const setupMoves = (number, pokemon) => {

    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

    fetch(BASE_URL + number).then((data) => {
      return data.json();
    })
    .then( (response) => {


        if(pokemon.getAttribute("id").charAt(0) == 1 && battleStarted){
          battleStarted = false;
          for(let i = 0; i < 4; i++){
            attackMoves1[i].setAttribute('opacity', '1');
            attackMoves1[i].setAttribute('value', response.moves[i].move.name);
            document.getElementById("20").setAttribute("opacity", 0);
            document.getElementById("30").setAttribute("opacity", 0);
            document.getElementById("40").setAttribute("opacity", 0);
            wildName[1].setAttribute("value", "");
            wildName[2].setAttribute("value", "");
            wildName[3].setAttribute("value", "");

            let att = document.createAttribute("animation");
            att.value = "property: position; easing: linear; dur: 1000; to: 2 4 -1 "
            pokemon.setAttribute("animation", att.value);

            let att2 = document.createAttribute("animation");
            att2.value = "property: position; easing: linear; dur: 1000; to: -2 3 -2 "
            starterBattle.setAttribute("animation", att2.value);
          }
        }
        if(pokemon.getAttribute("id").charAt(0) == 2 && battleStarted){
          battleStarted = false;
          for(let i = 0; i < 4; i++){
            attackMoves2[i].setAttribute('opacity', '1');
            attackMoves2[i].setAttribute('value', response.moves[i].move.name);
            document.getElementById("10").setAttribute("opacity", 0);
            document.getElementById("30").setAttribute("opacity", 0);
            document.getElementById("40").setAttribute("opacity", 0);
            wildName[0].setAttribute("value", "");
            wildName[2].setAttribute("value", "");
            wildName[3].setAttribute("value", "");

            let att = document.createAttribute("animation");
            att.value = "property: position; easing: linear; dur: 1000; to: 2 4 -1 "
            pokemon.setAttribute("animation", att.value);

            let att2 = document.createAttribute("animation");
            att2.value = "property: position; easing: linear; dur: 1000; to: -2 3 -2 "
            starterBattle.setAttribute("animation", att2.value);
          }
        }
        if(pokemon.getAttribute("id").charAt(0) == 3 && battleStarted){
          battleStarted = false;
          for(let i = 0; i < 4; i++){
            attackMoves3[i].setAttribute('opacity', '1');
            attackMoves3[i].setAttribute('value', response.moves[i].move.name);
            document.getElementById("20").setAttribute("opacity", 0);
            document.getElementById("10").setAttribute("opacity", 0);
            document.getElementById("40").setAttribute("opacity", 0);
            wildName[1].setAttribute("value", "");
            wildName[0].setAttribute("value", "");
            wildName[3].setAttribute("value", "");

            let att = document.createAttribute("animation");
            att.value = "property: position; easing: linear; dur: 1000; to: 2 4 -1 "
            pokemon.setAttribute("animation", att.value);

            let att2 = document.createAttribute("animation");
            att2.value = "property: position; easing: linear; dur: 1000; to: -2 3 -2 "
            starterBattle.setAttribute("animation", att2.value);
          };
        }
        if(pokemon.getAttribute("id").charAt(0) == 4 && battleStarted){
          battleStarted = false;
          for(let i = 0; i < 4; i++){
            attackMoves4[i].setAttribute('opacity', '1');
            attackMoves4[i].setAttribute('value', response.moves[i].move.name);
            document.getElementById("20").setAttribute("opacity", 0);
            document.getElementById("30").setAttribute("opacity", 0);
            document.getElementById("10").setAttribute("opacity", 0);

            let att = document.createAttribute("animation");
            att.value = "property: position; easing: linear; dur: 1000; to: 2 4 -1 "
            pokemon.setAttribute("animation", att.value);

            let att2 = document.createAttribute("animation");
            att2.value = "property: position; easing: linear; dur: 1000; to: -2 3 -2 "
            starterBattle.setAttribute("animation", att2.value);
          }
        }
    });
  }

  resetPokemon.onmouseenter = (event) => {
    let randomPokemonNumber = Math.floor(Math.random() * 151 + 1);
    let randomPokemonNumber2 = Math.floor(Math.random() * 151 + 1);
    let randomPokemonNumber3 = Math.floor(Math.random() * 151 + 1);

    getPokemon(randomPokemonNumber);
    getPokemon2(randomPokemonNumber2);
    getPokemon3(randomPokemonNumber3);
    resetPokemon.setAttribute("color", "green");
  }

  resetPokemon.onmouseleave = (event) => {
    resetPokemon.setAttribute("color", "red");
  }

  const getPokemon = (numberOfPokemon) => {
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    fetch(BASE_URL + numberOfPokemon).then((data) => {
      return data.json();
    })
    .then( (response) => {
      pokemonPicture.setAttribute("src", response.sprites.front_default);
      pokemonName.setAttribute("value", response.name);
    });
  }

  const getPokemon2 = (numberOfPokemon) => {
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    fetch(BASE_URL + numberOfPokemon).then((data) => {
      return data.json();
    })
    .then( (response) => {
      pokemonPicture2.setAttribute("src", response.sprites.front_default);
      pokemonName2.setAttribute("value", response.name);
    });
  }

  const getPokemon3 = (numberOfPokemon) => {
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";
    fetch(BASE_URL + numberOfPokemon).then((data) => {
      return data.json();
    })
    .then( (response) => {
      pokemonPicture3.setAttribute("src", response.sprites.front_default);
      pokemonName3.setAttribute("value", response.name);
    });
  }


  const getAttacks = (number, pokemon) =>{
    const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

    fetch(BASE_URL + number).then((data) => {
      return data.json();
    })
    .then( (response) => {;
      for(let i = 0; i < 4; i++){
        attackMoves5[i].setAttribute('opacity', '1');
        attackMoves5[i].setAttribute('value', response.moves[i].move.name);
  }
})
}

  for (let i = 0; i < pokemonStarterSprites.length; i++) {
      setupStarterPokemon(pokemonStarterSprites[i].getAttribute('id'), i);
      pokemonStarterSprites[i].onmouseenter = (event) =>{
        starterPokemon = pokemonStarterSprites[i].getAttribute('id');
        starterBattle.setAttribute('alt', pokemonStarterSprites[i].getAttribute('id'))
        console.log(starterBattle.getAttribute('alt'));
        let att = document.createAttribute("animation");
        att.value = "property: position; easing: linear; dur: 1000; to: 0 -6 0 "
        document.getElementById("starterPokemon").setAttribute("animation", att.value);

        let att2 =  "property: position; easing: linear; dur: 1000; to: 0 -1.2 -2 ";
        wildPokemon.setAttribute("animation", att2);

        pokemonStarterSprites[i].getAttribute('src');
        console.log(pokemonStarterSprites[i].getAttribute('value'));

        //starterBattle.setAttribute('src', pokemonStarterSprites[i].getAttribute('src'));
        starterBattle.setAttribute('alt', pokemonStarterSprites[i].getAttribute('id'));
        console.log("De alt is " + starterBattle.getAttribute('alt'));
        console.log(pokemonNames[i].getAttribute('value'));
        starterBattleName.setAttribute('value', pokemonNames[i].getAttribute('value'));
        getAttacks(starterBattle.getAttribute("alt"), starterBattle);

        const BASE_URL = "https://pokeapi.co/api/v2/pokemon/";

        fetch(BASE_URL + pokemonStarterSprites[i].getAttribute('id')).then((data) => {
          return data.json();
        })
        .then( (response) => {
          starterBattle.setAttribute("src", response.sprites.back_default);


        });


        setupWildPokemon();
    }
  }
}
